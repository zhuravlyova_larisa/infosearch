from pyelasticsearch import ElasticSearch

import argparse
import glob
import json
import os
import shutil
import subprocess
import sys

TEMP_DIR = 'youtube-dl-temp-dir'

def parse_args():
    parser = argparse.ArgumentParser(prog='youtube2es',
                                     description='Downloads subtitles from youtube '
                                     	          'videos and stores it in Elasticsearch')
    # optional
    parser.add_argument('-e',
                        '--es-host',
                        default='localhost',
                        action='store',
                        help='Elasticsearch host adress')

    # optional
    parser.add_argument('-p',
                        '--es-port',
                        default='9200',
                        action='store',
                        help='Elasticsearch port number')

	# positional
    parser.add_argument('playlist_urls',
                        nargs='*',
                        action='store',
                        default=[],
                        help='target playlist urls '
                        '(e.g., https://www.youtube.com/playlist?list=PLUl4u3cNGP60ONw6Q81tS64qKRUJ9nwq8)')

    args = parser.parse_args()

    return args


def main():
	args = parse_args()
	
	try:
		os.mkdir(TEMP_DIR)
	except OSError:
		print 'Temporary dir already exists.'
	
	cmd = ['youtube-dl', '--skip-download', '--write-sub',
		   '--write-info-json',
		   '-o', os.path.join(TEMP_DIR, '%(id)s.%(ext)s')]
	
	cmd.extend(args.playlist_urls)

	try:
		subprocess.check_call(cmd)
	except:
		print 'Youtube download error'
		exit(1)

	print
	print 'Subtitles downloaded. Load to Elasticsearch...'
	print

	es = ElasticSearch('http://%s:%s/' % (args.es_host, args.es_port))

	def process_sub_file(path):
		blocks = []

		basename = path[:path.find('.')]

		with open('%s.info.json' % basename) as metafile:
			meta = json.load(metafile)

			video = {
				"id": meta['id'],
				"title": meta['title'],
				"description": meta['description'],
				"playlist_title": meta['playlist_title'],
				"playlist_id": meta['playlist_id'],
			}
			
		with open(path) as sub:
			block = {}
			is_block_next = True
			for i, line in enumerate(sub):
				if i < 4: continue
				if is_block_next:
					block = {
						'time': line,
						'video': video['id'],
						'video_title': video['title'],
						'playlist': video['playlist_id'],
						'playlist_title': video['playlist_title'],
						'text' : '',
					}
					is_block_next = False
				elif not line.strip():
					is_block_next = True
					blocks.append(block)
				else:
					block['text'] += line
			if block:
				blocks.append(block)

		es.index('youtube',
				'video',
				video,
				id=video['id'])

		es.bulk(( es.index_op(block, id='%(video)s#%(time)s' % block) 
				  for block in blocks),
		        index='youtube',
		        doc_type='block')

	for sub_file in glob.glob(os.path.join(TEMP_DIR, '*.vtt')):
		print 'Processing file', sub_file
		process_sub_file(sub_file)

	print
	print 'deleting temporary files'
	shutil.rmtree(TEMP_DIR)

	print
	print 'DONE!'
	print

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print
		print 'Ctrl-C detected. Exit...'
		exit(0)